package com.stefan.rest_api_demo;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertEquals;


public class ManyToManyTest {

    private static SessionFactory sessionFactory;
    private Session session;

    @Test
    public void givenData_relationship() {
        String[] studentData = { "Peter Oven", "Allan Norman" };
        String[] courseData = { "IT Project", "Networking Project" };
        List<Curs> courses = new ArrayList<Curs>() {
        };

        for (String curs : courseData) {
            courses.add(new Curs(curs));
        }

        for (String stud : studentData) {
            Student student = new Student(stud.split(" ")[0],
                    stud.split(" ")[1]);

            assertEquals(0,student.getCourses().size());
            student.setCourses(courses);
            assertNotNull(student);
        }
    }

    @Test
    public void givenSession_whenRead_thenReturnsMtoMdata() {
        @SuppressWarnings("unchecked")
        List<Student> studentList = session.createQuery("SELECT * FROM Student")
                .list();

        assertNotNull(studentList);

        for(Student stud : studentList) {
            assertNotNull(stud.getCourses());
        }
    }
}
