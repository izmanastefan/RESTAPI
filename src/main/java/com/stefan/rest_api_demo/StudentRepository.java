package com.stefan.rest_api_demo;

import org.springframework.data.repository.CrudRepository;

public interface StudentRepository extends CrudRepository<Student,Integer>{
    public Student finByID(int id);
}
