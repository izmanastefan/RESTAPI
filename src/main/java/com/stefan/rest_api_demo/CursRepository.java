package com.stefan.rest_api_demo;

import org.springframework.data.repository.CrudRepository;

public interface CursRepository extends CrudRepository<Curs,Long> {
}
