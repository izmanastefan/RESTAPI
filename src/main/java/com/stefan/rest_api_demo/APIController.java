package com.stefan.rest_api_demo;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

@Controller
public class APIController {


    @Autowired
    StudentRepository studentRepository;

    @Autowired
    CursRepository cursRepository;

    @ResponseBody
    @RequestMapping(value = "/students", method = RequestMethod.POST)
    public String saveStudent(@RequestBody Student student) {
        studentRepository.save(student);
        return "SAVED";
    }

    @ResponseBody
    @RequestMapping(value = "/courses", method = RequestMethod.POST)
    public String saveCurs(@RequestBody Curs curs) {
        cursRepository.save(curs);
        return "SAVED";
    }

    @ResponseBody
    @RequestMapping(value = "/students")
    public List<Student> showStudent() {
        List<Student> list = (List<Student>) studentRepository.findAll();
        return list;
    }

    @ResponseBody
    @RequestMapping(value = "/courses")
    public List<Curs> showCourses() {
        List<Curs> list = (List<Curs>) cursRepository.findAll();
        return list;
    }


    @ResponseBody
    @RequestMapping(value = "/students/{ID}", method = RequestMethod.PUT)
    public Student editStudent(@Valid @RequestBody Student studentModif, @PathVariable("ID") String studentId) {

        Student student = studentRepository.findById(Long.parseLong(studentId)).orElseThrow(() -> new InvalidOpenTypeException());
        student.setFirstName(studentModif.getFirstName());
        student.setLastName(studentModif.getLastName());
        Student updateStudent = studentRepository.save(student);
        return updateStudent;
    }

    @ResponseBody
    @RequestMapping(value = "/courses/{ID}", method = RequestMethod.PUT)
    public Curs editCurs(@RequestBody Curs cursModif, @PathVariable("ID") String cursId) {
        Curs curs = cursRepository.findById(Long.parseLong(cursId)).orElseThrow(() -> new InvalidOpenTypeException());
        curs.setCourseName(cursModif.getCourseName());
        curs.setID(cursModif.getID());
        Curs updateCurs = cursRepository.save(curs);
        return updateCurs;
    }

    @ResponseBody
    @RequestMapping(value = "/students/{ID}")
    public Student getStudent(@PathVariable("ID") String studentId) {

        for (Student std : studentRepository.findAll()) {
            if (std.getID() == Integer.parseInt(studentId)) {
                return std;
            }
        }
        return null;

    }

    @ResponseBody
    @RequestMapping(value = "/courses/{ID}")
    public Curs getCurs(@PathVariable("ID") String cursId) {

        for (Curs curs : cursRepository.findAll()) {
            if (curs.getID() == Integer.parseInt(cursId)) {
                return curs;
            }
        }
        return null;

    }

    @ResponseBody
    @RequestMapping(value = "/students/{ID}", method = RequestMethod.DELETE)
    public String deleteStudent(@PathVariable("ID") String studentId) {
        for (Student student : studentRepository.findAll()) {
            if (student.getID() == Integer.parseInt(studentId)) {
                studentRepository.delete(student);
                return "Removed";
            }
        }
        return "Not Found";
    }

    @ResponseBody
    @RequestMapping(value = "/courses/{ID}", method = RequestMethod.DELETE)
    public String deleteCurs(@PathVariable("ID") String cursId) {
        for (Curs curs : cursRepository.findAll()) {
            if (curs.getID() == Integer.parseInt(cursId)) {
                cursRepository.delete(curs);
                return "Removed";
            }
        }
        return "Not Found";
    }

}
